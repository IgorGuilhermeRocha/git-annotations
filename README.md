# PRO-GIT
Este é apenas um projeto pessoal que guarda anotações importântes sobre o `git`, para que eu não esqueça. 

Se você busca um lugar para fazer `review` dos seus conhecimentos talvez isso possa te ajudar.Porém se você nunca viu nada de git, provavelmente essas anotações não farão sentido para você. Caso este seja o seu caso
leia diretamente o livro PRO-GIT disponível em: https://git-scm.com/book/pt-br/v2 

Eu escrevi essas anotações em inglês, caso não consiga entender vá logo aprender inglês. 

## Config
In this section, I will configure Git.

The `git config` command is used to perform core configuration on Git, such as setting your username and password.

This command has two main flags:

-   `--system`
-   `--global`

If you run `git config` with `--global`, the configuration will only be performed for your user, but if you use `--system`, the configuration will be valid for all users.

Running the `git config` command without a flag results in a configuration that is only valid for the project.

It works hierarchically, with the global level having priority over the system level, and the local level having priority over the global level.

### Examples:

#### Your identifier

Your email and name are used for recognize the commits and also other thinks. 

```bash
git config --global user.name 'IgorLikeAFuckingBoss'
git config --global user.email 'igorguilhermedev@gmail.com'
```

#### Your editor

this editor is used when you need to type some message like a commit for example, so you can change if you want, but the default editor is **vim**

```bash
$ git config --global core.editor emacs
```

#### Checking settings

For checking your settings tun this:

```bash
git config --list
```

You also can check specific values like:

```bash
git config user.name
```

## Starting with git

For starting a new project we can use `git init` or `git clone` if the project already exists.

### Git init

Just go to your workspace and use `git init`, that create a new folder named .git, this folder is reponsible for tracking your files.

```bash
cd deep_learning_git
git init
touch README.md
git add README.ms
git commit -m 'first commit'
```

### Git clone

The syntax is really simples just type `git clone < url >` in your bash.

```bash
git clone https://gitlab.com/IgorGuilhermeRocha/blackjack
```

You also can do `git clone < url > < new name >`, to give a new name for root folder of your project.

```bash
git clone https://gitlab.com/IgorGuilhermeRocha/blackjack whitejack
```
## States of the files

Every file in git has one or more states,  they are :

- Untracked
- <span style="color: blue;">Unmodfied</span>
- <span style="color:yellow;">Modified</span>
- <span style="color:orange;">Staged </span>

The comand  `git status ` shows these states.

### <span style="color: blue;">Unmodfied</span>

This state means the file has not been changed compared to its previous version.

```bash
cd existing_project
git status
On branch main
nothing to commit, working tree clean
```


### Untracked 

That state means the file is new, in other words that file is not present in older versions of project.

```bash
touch untracked_file.txt
git status
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	untracked_file.txt

```

In this terminal above, i change to my project and i use `git status` to check the state of my files, its everything ok, after that i create a new file named `untracked_file` so i use `git status` again and now i have a message that shows me i have an Untracked file.

### <span style="color:yellow;">Modified</span> and <span style="color:orange;">Staged </span>

Modified means the file has been changed but these changes have not been staged yet and Staged means the changes have not been commited yet.

```bash 
git add untracked_file.txt 
vim untracked_file.txt #write something in 
git status
On branch main
Changes to be committed: # changes in stage
  (use "git restore --staged <file>..." to unstage)
	new file:   untracked_file.txt

Changes not staged for commit: # changes to be staged
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   untracked_file.txt

```

Alternatively you can use `git status -s` to have a more simple response about the states of files.

```
git status -s
AM untracked_file.txt
```

- A : Added to the Git index (staging area) and ready to be committed.
- M : Modified and ready to be committed.
- ?? : Untracked file, not yet added to the Git index.

## Skipping Stage

To add a file to the stage, normally we use the `git add` command. However, we can skip the stage by using the `-a` flag on the `git commit` command.

Normal mode:

```bash
git add file.txt
git commit -m "Your commit message"
```

With the flag:,

```bash
git commit -a -m "Your commit message"
```

### Deleting a file with git

To delete a file using Git, run the following command:

```bash
git rm <file_name>
```

When you use this command, the deletion will be automatically staged. Also you can follow the normal way.

```bash
rm <file_name>
git add <file_name>
```

You can use the `--cached` flag with the `git rm` command to remove a file from the index in a Git repository, while keeping it on your hard disk. The file will no longer be tracked by Git, but it remains on your local file system.


### Moving a file with git

This will be a short text, that syntax is like a `git rm` command.

```bash
git mv <source> <destination>
```
## See differences 
### Git diff

`git diff` is a very useful command because it shows the difference between a modified file and the file in the stage, as well as the difference between staged files and the last committed files.

```bash
cd Desktop/deep_learning_git
git status
On branch main
nothing to commit, working tree clean
touch new_file.txt

git status
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	new_file.txt
	
git add .
vim new_file.txt
git status
On branch main
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   new_file.txt

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   new_file.txt
	
git diff
diff --git a/new_file.txt b/new_file.txt
index e69de29..14ca8b9 100644
--- a/new_file.txt
+++ b/new_file.txt
@@ -0,0 +1 @@
+new_content

```

In the example above, a new file called *new_file.txt* was created and added to the stage using the `git add .` command. Then, the file was modified and the `git diff` command was used to show the difference between the modified version of `new_file.txt` and its staged version. 

We can compare the changes between stage and last commit to using flag `--staged` or `--cached`:

```bash
git diff --cached
#or
git diff --staged
```

Also we can use git diff for a specific file:

```bash
git diff <name_of_file> 
```
## View commits

You can view commits with command `git log`. This comand have so many tags they are:

- `--patch` or `-p`, to see the changes;
- -< number >, limit the number of the commits;
- --stat, is like a short patch;
- --pretty
	- oneline
	- short, full and fuller, all levels show more or less information.
	- format, you can specify the format that you want.
- --graph , this add a nice Graph if you want to see the branchs.

### Searching for commits in a specific date

```bash
git log --since=2.weeks
```

## Undoing

In this section you will learn about `git reset`, `git restore` , `git checkout` and flag `amend`

### amend

You can use the flag `--amend` when you forget to put some files to stage, basically you overrite the previous commit.

```bash
touch some_file.txt
touch forgotten_file.txt
git add  some_file.txt
git commit -m 'adding some files'
git add forgotten_file.txt
git commit --amend
```

### reset and checkout

Using reset you can easily unstage a file.

```bash
touch some_file.txt
git add .
git status
On branch new_branch
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	deleted:    file_for_example_2.txt


git reset HEAD some_file.txt
Unstaged changes after reset:
D	file_for_example_2.txt

git status
On branch new_branch
Changes not staged for commit:
  (use "git add/rm <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	deleted:    file_for_example_2.txt

```

Also you can use `git checkout -- ` to revert modifications in file.

```bash
git checkout -- file_for_example_2.txt
git status
On branch new_branch
nothing to commit, working tree clean
```

### git restore

With `git restore` you can do the function of both `git reset` and `git checkout --`, using the flag `--staged` you retrive a file of stage and use without you revert modifications.

```bash
touch file_to_be_reverted.txt
git add .
git restore --staged file_to_be_reverted.txt
vim file_to_be_reverted.txt #write something
git restore file_to_be_reverted
```
## Branch Basics

### Concepts

In this section, we will learn about the basics of branching, creating, deleting, switching, and merging.

A branch is a new pathway of your project. When you need to create a new feature, it is good practice to create a new branch for it. This helps to avoid altering your current working code and keeps your changes separate until you are ready to merge them into the main codebase.

### Create

You can create branches with the below code.

```bash
git branch < name of branch >
```

If you need to check which branch your local repository is working on, you can run a command to do so.

```bash
git log --oneline --decorate
```


### Switch

To switch to a different branch, we typically use the `git checkout` or `git switch` command.

```bash
git checkout < name of branch >
#or
git switch < name of branch >
```

Both `git checkout` and `git switch` can perform the same action of switching to a different branch, but they have some differences.

One difference is that you cannot use `git checkout` if you have uncommitted changes in your working directory. In that case, you need to either commit or stash your changes first.

Another difference is that `git checkout` has more functionalities besides just switching branches, while `git switch` is mainly designed for switching branches.

To create a new branch using `git checkout`, you can add the `-b` flag followed by the name of the new branch.

```bash
git checkout -b < name of branch >
```

### See branches with git log

You can use the `git log` command with the following flags:

-   `--decorate`: this shows which branch the HEAD is currently pointing to.
-   `--oneline`: this flag shows the commit messages in a more condensed format.
-   `--graph`: this flag adds a visual representation of the branch history to improve comprehension.
-   `--all`: this flag shows the commits across all branches, rather than just the current branch.

```bash
git log --decorate --oneline --graph --all

* 5948361 (HEAD -> learning, origin/main, main, learningBranches) anything here
* 36ab944 (tag: v1.0.3-lw, tag: v1.0.2, tag: v1.0.1, tag: v1.0.0) Delete some_file.txt
*   4dede37 Merge branch 'main' of https://gitlab.com/IgorGuilhermeRocha/pro-git
|\  
| * 383b835 Delete README.md
* | 23e9329 (new_branch) adding some files
|/  
* 701b1fd O my god
* e1962cd .gitignore
* d594fc0 just dont important
* ae2583a nothing interisthing here
* 890e25c just for studying
* 893b74e states example
* c28fbac First commit


```


### Merge

Ok, in this part i will supose that you have a project and need to do a new feature for it.

#### Creating a new branch 

```bash
git switch -c newFeature
```

#### Do your modifications

In this example above i modify a file and commit my changes. After all i change to branch learning.

```bash
vim new_file_in_new_branch.txt
git add . && git commit -m 'new feature has been add'
git checkout learning
```

#### Git merge

to merge a branch `learning` with `newFeature` , i will use the command `git merge`.

```bash
git merge newFeature
```

After that, my branch `learning` is up to date with `newFeature`.


### Deleting branches

It is a good practice to delete branches after they have been merged, especially if you are using the `main` or `master` branch to keep your stable, bug-free code. Once you have merged your new feature or bugfix into the main branch, you no longer need the feature branch. Keeping old feature branches around can clutter your repository and make it harder to find the branches you actually need. Additionally, deleting old branches can help you avoid accidentally merging old, unfinished code in the future.

For delete you can use:

```bash
git checkout -d < name of branch >
git branch -d < name of branch >
```

### Merge conflicts

Let's say two friends, Igor Guilherme and Rediberto, are working on the same project.

Igor Guilherme creates a file and pushes it to the main branch in the remote repository. Rediberto pulls the project and starts editing the same file. 

However, while Rediberto is editing the file, Igor Guilherme also makes some changes and pushes them to the remote repository. 

Now, Rediberto has outdated code and may run into conflicts when trying to merge his changes with the updated version of the file. 


```bash
#igor doing your stuffs
touch merge_conflicts.txt
vim  merge_conflicts.txt
git add .
git commit -m 'new file'
git push
```

```bash
#rediberto
git pull
vim  merge_conflicts.txt
git push
# in the same time igor
vim  merge_conflicts.txt
git push
```


When the other person try to merge your changes, may he will got a merge conflict.

```bash
 Auto-merging merge_conflicts.txt
CONFLICT (content): Merge conflict in merge_conflicts.txt
Automatic merge failed; fix conflicts and then commit the result.

```

```bash
git status
You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
	both modified:   merge_conflicts.txt
cat merge_conflicts.txt 
<<<<<<< HEAD
dasdsadsdsaadsadnsdjjiZZ
=======
dasdsadsdsdsdd
>>>>>>> main2

```

You can exclude the part of the file that you dont want in merge.
HEAD -> current,
main2 -> other branch.

## Branch management

To list branches you can use `git branch`

```bash
git branch
  learning
  learningBranches
* main
  main2
  new_branch

```

The branch that your `HEAD` is currently pointing to is indicated by a `*` symbol in the output of the `git branch` command.

Also you can use the same command with some many tags like:

- -v , this tag shows the last commit of each branch.
- --vv, for more info.
- --all, with this, we can see all branches, even the branches that was present just in remote repository;
- --merged, this flag shows the branches that the current branch already merged;
- --no-merged, the opposite of the --merged;
- -d to remove ;
- -D to force removing;
- --move to change the branch name

### Examples

```bash

git branch
 learning
  learningBranches
* main
  main2
  new_branch

git branch -v
  learning         e9067d2 new feature has been add
  learningBranches 5948361 anything here
* main             e65ee8e merge commit
  main2            a675b25 just here
  new_branch       23e9329 adding some files

git branch --all # in this case i dont have any branch in remote that i havent in local

  learning
  learningBranches
* main
  main2
  new_branch
  remotes/origin/main # this is means that branch is present in remote to
  remotes/origin/main2

git branch -d main2
warning: deleting branch 'main2' that has been merged to
         'refs/remotes/origin/main2', but not yet merged to HEAD.
Deleted branch main2 (was a675b25).

git branch -d learning
error: The branch 'learning' is not fully merged.
If you are sure you want to delete it, run 'git branch -D learning'.

git branch -D learning
Deleted branch learning (was e9067d2).


git branch 
  learningBranches
* main
  new_branch

git branch --merged
 learningBranches
* main
  new_branch

git branch --no-merged

git branch --move new_branch master

git branch
  learningBranches
* main
  master


```

OBS: When you delete a branch with -d flag, the branch is only deleted in your local computer, for delete a branch in remote repository run this `git  push --delete < branch name >` and when you rename a branch using ` --move `  you need to run `git push -u origin < branch name >` to your local modifcations take effect in remote to.




## Remotes

In most cases, you need to work with others, so you can use remote repositories such as GitHub, GitLab, AWS CodeCommit, or Bitbucket to facilitate collaboration.

In this section you will earn about remotes.

### Add a remote

To add a remote repository, use the `git remote add` command followed by the remote name and URL. The remote name is often `origin`

```bash
git remote add origin https://gitlab.com/IgorGuilhermeRocha/pro-git
```

To view existing remotes, use the `git remote` command. To show more information, use the `-v` flag.

```bash
git remote
origin

# or -v flag to show more information
git remote -v
origin	https://gitlab.com/IgorGuilhermeRocha/pro-git (fetch)
origin	https://gitlab.com/IgorGuilhermeRocha/pro-git (push)

```

You can inspect your repository using the `git remote show` command.

```bash
git remote show origin
```

### Rename and removing remotes

Rename with

```bash
git remote rename origin gitlab
```

Removing with

```bash
git remote remove origin
```
## Remote branches

Remote branches are branches in a remote repository like:

- github
- gitlab

In this section you will learn about remote branches.

### Show yours remotes

```bash
git remote
# or
git remote -v # with this flag you can see the URL of remote.
```

If you need more information, you can use the commands : `git remote show < name of remote>` or `git ls-remote < name of remote >`

The name `origin` is default, but you can change for whatever name that you want. For example using the  flag `-o`  on command `git clone` you can specify a name for this remote.

```bash
git clone -o thisIsMyRemote https://gitlab.com/IgorGuilhermeRocha/chess-game
# changind directory
git remote
thisIsMyRemote

```

### Add remotes

```bash
git remote add < name of remote > < url >
```

### Fetch remotes

```bash
git fetch < name of remote > < name of branch>
git fetch --all # to download all content
```

### git pull

Pull is just a command to do fetch and merge.

```bash
git pull
git pull < remote > < branch >
```

### Push and delete

```bash
git push < remote > < branch >
# or if you want to have different branch names in local and remote
git push < remote > <localbranchname:remoteBranchname>

git push < remote > --delete < branch >
```

## Tags

### View tags

```bash
git tag
```

With a pattern:

```bash
git tag -l "v1.8.5"
```

### Create tags

#### Lightweight tags

this kind of tags is petty simple, just a pointer for a specific commit.

```bash
git tag < name_of_tag >
```

#### Annoted tags

## Protocols

We have 4 options of protocols to transfer files in git.

-   Local -> This is used in local networks of enterprises but you may never use it.
-   HTTPS -> This is the most commonly used protocol as it is simple and efficient.
-   SSH -> This is the fastest and most secure option, but authentication can be tedious.
-   GIT -> This is the fastest option and doesn't require authentication.

If for some reason you need to configure your own server. it is good to read this topics of PRO-GIT:

- https://git-scm.com/book/en/v2/Git-on-the-Server-Getting-Git-on-a-Server
- https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key
- https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server
- https://git-scm.com/book/en/v2/Git-on-the-Server-Git-Daemon
- https://git-scm.com/book/en/v2/Git-on-the-Server-Smart-HTTP
- https://git-scm.com/book/en/v2/Git-on-the-Server-GitWeb
- https://git-scm.com/book/en/v2/Git-on-the-Server-GitLab
- https://git-scm.com/book/en/v2/Git-on-the-Server-Third-Party-Hosted-Options


## Stash
You can use the command `git stash` to save your changes without commit then.

### Example

Stashing your changes
```bash
git status
On branch main
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   src/pages/Home/index.tsx

git stash
Saved working directory and index state WIP on main: 138517a beta

git status
On branch main
nothing to commit, working tree clean

git stash list
stash@{0}: WIP on main: 138517a beta

```

Apply your changes.

```bash
git stash apply
# also
git stash pop
```

cleaning

```bash
git stash clean
```








